module gitlab.com/felipemocruha/yarb

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/golang/protobuf v1.2.0
	golang.org/x/net v0.0.0-20180826012351-8a410e7b638d
	google.golang.org/grpc v1.16.0
	gopkg.in/yaml.v2 v2.2.1
)
