FROM alpine:3.8

RUN apk --update --no-cache add ca-certificates tzdata curl shadow \
	&& cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
    && echo "America/Sao_Paulo" > /etc/timezone \
    && groupadd -r default \
    && useradd --no-log-init -r -g default default 

COPY build/recipes-server /usr/bin
USER default

CMD recipes-server -a ${SERVER_ADDRESS} \
				   -d ${DB_URL} \
				   -c ${CERT_PATH} \
				   -k ${KEY_PATH}
