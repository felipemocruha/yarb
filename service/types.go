package main

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Service struct {
	DB *mgo.Session
}

type Config struct {
	ServicePort string
	DatabaseURL string
}

type Ingredient struct {
	Name     string `bson:"name"`
	Quantity string `bson:"quantity"`
}

type Recipe struct {
	ID          bson.ObjectId `bson:"_id"`
	Name        string        `bson:"name"`
	Ingredients []Ingredient  `bson:"ingredients"`
}
