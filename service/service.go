package main

import (
	"context"
	"math/rand"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	pb "gitlab.com/felipemocruha/yarb/recipes"
)

func modelToRequest(recipe Recipe) *pb.RecipeRequest {
	var ingredients []*pb.Ingredient

	for _, ing := range recipe.Ingredients {
		ingredients = append(ingredients, &pb.Ingredient{
			Name: ing.Name,
			Quantity: ing.Quantity,
		})
	}

	return &pb.RecipeRequest{
		Id:          recipe.ID.Hex(),
		Name:        recipe.Name,
		Ingredients: ingredients,
	}
}

func requestToModel(req *pb.RecipeRequest) Recipe {
	var ingredients []Ingredient

	for _, ing := range req.Ingredients {
		ingredients = append(ingredients, Ingredient{
			Name: ing.Name,
			Quantity: ing.Quantity,
		})
	}

	recipe := Recipe{
		Name:        req.Name,
		Ingredients: ingredients,
	}
	
	var id bson.ObjectId
	if req.Id != "" {
		id = bson.ObjectIdHex(req.Id)
		recipe.ID = id
	}
	
	return recipe
}

func (s *Service) GetRecipes(_ *pb.RecipeFilter, stream pb.RecipeBook_GetRecipesServer) error {
	session := s.DB.Copy()
	defer session.Close()

	recipes := []Recipe{}
	coll := session.DB("recipe_book").C("recipe")

	if err := coll.Find(nil).All(&recipes); err != nil {
		return err
	}

	for _, recipe := range recipes {
		req := modelToRequest(recipe)

		if err := stream.Send(req); err != nil {
			return err
		}
	}

	return nil
}

func getRecipeByID(s *mgo.Session, id string) (Recipe, error) {
	session := s.Copy()
	defer session.Close()

	coll := session.DB("recipe_book").C("recipe")
	recipe := Recipe{}

	if err := coll.FindId(bson.ObjectIdHex(id)).One(&recipe); err != nil {
		return recipe, err
	}

	return recipe, nil
}

func getRecipeByName(s *mgo.Session, name string) (Recipe, error) {
	session := s.Copy()
	defer session.Close()

	coll := session.DB("recipe_book").C("recipe")
	recipe := Recipe{}

	if err := coll.Find(bson.M{"name": name}).One(&recipe); err != nil {
		return recipe, err
	}

	return recipe, nil
}

func getRecipeByIngredient(s *mgo.Session, ingredient string) (Recipe, error) {
	session := s.Copy()
	defer session.Close()

	coll := session.DB("recipe_book").C("recipe")
	recipe := []Recipe{}

	if err := coll.Find(bson.M{"ingredients.name": ingredient}).All(&recipe); err != nil {
		return Recipe{}, err
	}

	if len(recipe) > 0 {
		idx := randInt(0, len(recipe))
		return recipe[idx], nil
	}

	return Recipe{}, nil
}

func (s *Service) GetRecipe(ctx context.Context, filter *pb.RecipeFilter) (*pb.RecipeRequest, error) {
	recipe := Recipe{}
	var err error
	
	if filter.Id != "" {
		recipe, err = getRecipeByID(s.DB, filter.Id)
		if err != nil {
			return &pb.RecipeRequest{}, err
		}

	} else if filter.Name != "" {
		recipe, err = getRecipeByName(s.DB, filter.Name)
		if err != nil {
			return &pb.RecipeRequest{}, err
		}

	} else if filter.Ingredient != "" {
		recipe, err = getRecipeByIngredient(s.DB, filter.Ingredient)
		if err != nil {
			return &pb.RecipeRequest{}, err
		}
	}

	return modelToRequest(recipe), nil
}

func randInt(min int, max int) int {
	return min + rand.Intn(max - min)
}

func (s *Service) GetRandomRecipe(ctx context.Context, filter *pb.RecipeFilter) (*pb.RecipeRequest, error) {
	recipes := []Recipe{}

	session := s.DB.Copy()
	defer session.Close()

	coll := session.DB("recipe_book").C("recipe")
	if err := coll.Find(nil).All(&recipes); err != nil {
		return &pb.RecipeRequest{}, err
	}

	if len(recipes) > 0 {
		idx := randInt(0, len(recipes))
		return modelToRequest(recipes[idx]), nil
	}
	
	return &pb.RecipeRequest{}, nil
}

func (s *Service) AddRecipe(ctx context.Context, req *pb.RecipeRequest) (*pb.RecipeResponse, error) {
	session := s.DB.Copy()
	defer session.Close()

	coll := session.DB("recipe_book").C("recipe")
	recipe := requestToModel(req)
	recipe.ID = bson.NewObjectId() 
	
	return &pb.RecipeResponse{}, coll.Insert(recipe)
}

func (s *Service) RemoveRecipe(ctx context.Context, filter *pb.RecipeFilter) (*pb.RecipeResponse, error) {
	session := s.DB.Copy()
	defer session.Close()
	coll := session.DB("recipe_book").C("recipe")

	return &pb.RecipeResponse{}, coll.RemoveId(bson.ObjectIdHex(filter.Id))
}
