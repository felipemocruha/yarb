package main

import (
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"net"
	"time"
	"flag"

	"github.com/globalsign/mgo"
	"google.golang.org/grpc/credentials"
	pb "gitlab.com/felipemocruha/yarb/recipes"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	
	serviceAddress := flag.String("a", ":8080", "address which the server will listen")
	dbURL := flag.String("d", "localhost:27017", "database URL")
	keypath := flag.String("k", "../cert/localhost.key", "filepath to credentials key")
	certpath := flag.String("c", "../cert/localhost.crt", "filepath to credentials certificate")
	flag.Parse()
	
	listener, err := net.Listen("tcp", *serviceAddress)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	dbSession, err := mgo.Dial(*dbURL)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	service := grpc.NewServer(grpc.Creds(getCredentials(*keypath, *certpath)))
	pb.RegisterRecipeBookServer(service, &Service{DB: dbSession})

	log.Printf("Starting server at %s", *serviceAddress)
	service.Serve(listener)
}

func getCredentials(keypath, certpath string) credentials.TransportCredentials {
	creds, err := credentials.NewServerTLSFromFile(certpath, keypath)
	if err != nil {
		log.Fatalf("Failed to setup TLS: %v", err)
	}

	return creds
}
