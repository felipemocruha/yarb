protoc:
	protoc -I recipes recipes/recipes.proto --go_out=plugins=grpc:recipes

run_server:
	cd service && go build -i -o recipes-server && ./recipes-server

build_server:
	cd server && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ../build/recipes-server .

build_client:
	cd client && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ../build/recipes .

cert_dev:
	certstrap --depot-path cert init --common-name "localhost"

mongo:
	docker run -d -p 27017:27017 mongo:4

.PHONY: protoc run_server build_server run_client build_client
