package main

type Recipe struct {
	Name        string   `yaml:"name"`
	Ingredients []string `yaml:"ingredients"`
}
