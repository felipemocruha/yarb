package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"io/ioutil"

	pb "gitlab.com/felipemocruha/yarb/recipes"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	address := flag.String("a", defaultAddress(), "server address")
	list := flag.Bool("l", false, "list all recipes")
	random := flag.Bool("rand", false, "gets a random recipe")
	file := flag.String("f", "", "path to recipe yaml file")
	remove := flag.String("r", "", "removes the recipe with provided ID")
	id := flag.String("id", "", "gets a recipe by id")
	name := flag.String("n", "", "gets a recipe by name")
	ingredient := flag.String("ing", "", "gets some recipe with provided name")
	cert := flag.String("c", defaultPath(), "path to TLS credentials")
	flag.Parse()

	creds := getCredentials(*cert)
	conn, err := grpc.Dial(*address, grpc.WithTransportCredentials(creds))
	if err != nil {
		fmt.Errorf("Server connection failed: %v", err)
		os.Exit(1)
	}
	defer conn.Close()
	client := pb.NewRecipeBookClient(conn)

	if *list {
		ListRecipes(client)

	} else if *random {
		GetRandomRecipe(client)

	} else if *file != "" {
		AddRecipe(client, *file)

	} else if *id != "" {
		GetRecipeByID(client, *id)

	} else if *name != "" {
		GetRecipeByName(client, *name)

	} else if *ingredient != "" {
		GetRecipeByIngredient(client, *ingredient)

	} else if *remove != "" {
		RemoveRecipe(client, *remove)
	}

	fmt.Println("At least one of the flags must be specified. Try again moron.")
	os.Exit(1)
}

func getCredentials(path string) credentials.TransportCredentials {
	creds, err := credentials.NewClientTLSFromFile(path, "")
	if err != nil {
		fmt.Printf("Failed to setup TLS: %v\n", err)
	}

	return creds
}

func defaultPath() string {
	return path.Join(os.Getenv("HOME"), ".recipes/localhost.crt")
}

func defaultAddress() string {
	file := path.Join(os.Getenv("HOME"), ".recipes/address")

	raw, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Printf("Failed to address config: %v\n", err)
		os.Exit(1)
	}

	return string(raw)
}
