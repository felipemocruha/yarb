package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	pb "gitlab.com/felipemocruha/yarb/recipes"
	"gopkg.in/yaml.v2"
)

func printRecipe(recipe *pb.RecipeRequest) {
	fmt.Println("*=============RECIPE BOOK=============*")
	fmt.Printf("\nName: %v\n", strings.Title(recipe.Name))
	fmt.Printf("Ingredients:\n")

	for _, ing := range recipe.Ingredients {
		fmt.Printf("\t- %v -> |%v|\n", ing.Name, ing.Quantity)
	}

	fmt.Println("")
}

func ListRecipes(client pb.RecipeBookClient) {
	stream, err := client.GetRecipes(context.Background(), &pb.RecipeFilter{})
	if err != nil {
		fmt.Println("Failed to list recipes: %v", err)
		os.Exit(1)
	}

	fmt.Println("NAME                                ID")
	for {
		r, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Printf("Failed recovering recipes: %v", err)
			os.Exit(1)
		}

		fmt.Printf("%v                               %v\n", r.Name, r.Id)
	}

	os.Exit(0)
}

func GetRandomRecipe(client pb.RecipeBookClient) {
	recipe, err := client.GetRandomRecipe(context.Background(), &pb.RecipeFilter{})
	if err != nil {
		fmt.Println("Failed to get a random recipe: %v", err)
		os.Exit(1)
	}

	printRecipe(recipe)
	os.Exit(0)
}

func GetRecipeByID(client pb.RecipeBookClient, id string) {
	recipe, err := client.GetRecipe(context.Background(), &pb.RecipeFilter{Id: id})
	if err != nil {
		fmt.Println("Failed to get recipe: %v", err)
		os.Exit(1)
	}

	printRecipe(recipe)
	os.Exit(0)
}

func GetRecipeByName(client pb.RecipeBookClient, name string) {
	recipe, err := client.GetRecipe(context.Background(), &pb.RecipeFilter{Name: name})
	if err != nil {
		fmt.Println("Failed to get recipe: %v", err)
		os.Exit(1)
	}

	printRecipe(recipe)
	os.Exit(0)
}

func GetRecipeByIngredient(client pb.RecipeBookClient, ing string) {
	recipe, err := client.GetRecipe(context.Background(), &pb.RecipeFilter{Ingredient: ing})
	if err != nil {
		fmt.Println("Failed to get some recipe: %v", err)
		os.Exit(1)
	}

	printRecipe(recipe)
	os.Exit(0)
}

func RemoveRecipe(client pb.RecipeBookClient, id string) {
	fmt.Println("Are you sure you want to remove it?")
	fmt.Printf("[y/N]: ")
	var opt string
	fmt.Scanf("%s", &opt)

	if opt != "y" {
		fmt.Println("Canceled!")
		os.Exit(0)
	}

	if _, err := client.RemoveRecipe(context.Background(), &pb.RecipeFilter{Id: id}); err != nil {
		fmt.Println("Failed to remove recipe: %v", err)
		os.Exit(1)
	}

	fmt.Println("Reciped removed!")
	os.Exit(0)
}

func AddRecipe(client pb.RecipeBookClient, filepath string) {
	req, err := readFile(filepath)
	if err != nil {
		fmt.Printf("Failed to add recipe: %v", err)
		os.Exit(1)
	}

	if _, err = client.AddRecipe(context.Background(), req); err != nil {
		fmt.Printf("Failed to add recipe: %v", err)
		os.Exit(1)
	}

	fmt.Println("Recipe added!")
	os.Exit(0)
}

func readFile(filepath string) (*pb.RecipeRequest, error) {
	raw, err := ioutil.ReadFile(filepath)
	if err != nil {
		msg := fmt.Sprintf("Failed to read file: %v", err)
		return &pb.RecipeRequest{}, errors.New(msg)
	}

	recipe := Recipe{}
	if err = yaml.Unmarshal(raw, &recipe); err != nil {
		msg := fmt.Sprintf("Failed to decode file: %v", err)
		return &pb.RecipeRequest{}, errors.New(msg)
	}

	req, err := parseRecipe(recipe)
	if err != nil {
		msg := fmt.Sprintf("Invalid syntax: %v", err)
		return &pb.RecipeRequest{}, errors.New(msg)
	}

	return req, nil
}

func parseRecipe(recipe Recipe) (*pb.RecipeRequest, error) {
	req := &pb.RecipeRequest{
		Name: recipe.Name,
	}

	for _, ing := range recipe.Ingredients {
		s := strings.Split(ing, " ")
		if len(s) < 2 {
			return req, errors.New("A recipe item must have an ingredient and a quantity.\ne.g. farinha de trigo 100g")
		}

		req.Ingredients = append(req.Ingredients, &pb.Ingredient{
			Name:     strings.Join(s[:len(s)-1], " "),
			Quantity: s[len(s)-1],
		})
	}

	return req, nil
}
